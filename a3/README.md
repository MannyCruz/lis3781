> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### Assignment 3 Requirements:


1.  Provide screenshots of sql code and having run the statements to yield meaningful result sets.
2.  Provide screenshots of at least a few of those statements.
3.  Add numbers to the left margin of the statement lines.


#### Screenshots of sql code and company/customer list_tables_admin

#### Aggregate Values
  ![Screenshot of aggregate values](img/a3_display_agg_vals.png)

#### Formatted Commodity Prices
  ![Screenshot of commodity prices](img/a3_display_comm_price_format.png)

#### Customer ID, full name, email displayed
  ![Screenshot of cus details](img/a3_display_cus.png)

#### Current date-time displayed
  ![Screenshot of date-time](img/a3_display_datetime.png)

#### User privileges
  ![Screenshot of user privs](img/a3_display_privs.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
