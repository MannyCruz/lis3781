> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install AMPPS
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions

2.  [A2 README.md](a2/README.md "My A2 README.md file")
    * Provide screenshots of sql code
    * Provide screenshots of populated company and customer tables
    * Create 2 users with distinct privileges

3.  [A3 README.md](a3/README.md "My A3 README.md file")
    * Screenshot of your SQL code used to create and populate your tables;
    * Screenshot of your populated tables (w/in the Oracle environment);
    * Optional: SQL code for a few of the required reports.
    * Bitbucket repo links: *Your* lis3781 Bitbucket repo link

4.  [A4 README.md](a4/README.md "My A4 README.md file")
    * ERD (tables *must* be populated using RemoteLabs – MS SQL Server)
    * SQL Statement Questions
    * Screenshot of *your* ERD;
    * Optional: SQL code for the required reports.
    * Bitbucket repo links: *Your* lis3781 Bitbucket repo link

5.  [A5 README.md](a5/README.md "My A5 README.md file")
    * Screenshot of *your* ERD;
    * Screenshot: At least *one* required report (i.e., exercise below), and SQL code solution.
    * Bitbucket repo links: *Your* lis3781 Bitbucket repo link

6.  [P1 README.md](p1/README.md "My P1 README.md file")
    * ERD (in .mwb format, .png files will *not* be accepted)
    * SQL Statement Questions

7.  [P2 README.md](p2/README.md "My P2 README.md file")
    * Screenshot of at least one MongoDB shell command(s), (e.g., show collections);
    * Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.  
    * Bitbucket repo links: *Your* lis3781 Bitbucket repo link




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
