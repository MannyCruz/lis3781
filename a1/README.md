> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### Assignment 1 Requirements:

*Five Parts:*

1.  Distributed Version Control with Git and Bitbucket

2.  AMPPS Installation
3.  Questions
4.  Entity Relationship Diagram and SQL Code (optional)
5.  Bitbucket repo links:
    a) this assignment and
    b) the completed tutorial (bitbucketstationlocations)


## A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees' histories must be tracked. Also, include the following business rules:

  * Each employee may have one or more dependents.

  * Each employee has only one job.

  * Each job an be held by many employees.

  * Many employees may receive many benefits.

  * Many benefits may be selected by many employees (though, while they may not select any benefits - any dependents of employees may be on an employee's plan).
  Notes:

  * Employee/dependent tables must use suitable attributes (See assignment guidelines);

In addition:

  * Employee SSN, DOB, start/end dates, salary;

  * Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc.

  * Job: title (e.g., secretary, service tech, manager, cashier, janitor, IT, etc)

  * Benefit: name (e.g. medical, dental, long-term disability, 401k, term life insurance, etc.)

  * Plan: type (single,spouse,family), cost, election date (plans must be unique)

  * Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;

  * Zero filled data: SSN, zip codes, (not phone numbers);

  * *All* tables must include notes attribute.



  **README.md file should include the following items:**

Screenshot of A1 ERD

Ex1. SQL Solution

git commands w/short descriptions

  **Git commands w/short descriptions:**

  1. git init - Create an empty Git repository or reinitialize an existing one
  2. git status - Show the working tree status
  3. git add - Add file contents to the index
  4. git commit - Record changes to the repository
  5. git push - Update remote refs along with associated objects
  6. git pull - Fetch from and integrate with another repository or a local branch
  7. git merge - Join two or more development histories together

  **Assignment Screenshots:**

#### Screenshot of A1 ERD

  ![A1 ERD](img/lis3781_a1.png)


#### Screenshot of Successful mysql login

  ![Screenshot of mysql login](img/lis3781_a1_proof_of_signin.png)


#### Screenshots of backward-engineered query result sets (1-7)

#### Query Result Set 1
  ![Screenshot of back-eng queries](img/lis3781_a1_q1.png)
#### Query Result Set 2
  ![Screenshot of back-eng queries](img/lis3781_a1_q2.png)
#### Query Result Set 3
  ![Screenshot of back-eng queries](img/lis3781_a1_q3.png)
#### Query Result Set 4
  ![Screenshot of back-eng queries](img/lis3781_a1_q4.png)
#### Query Result Set 5
  ![Screenshot of back-eng queries](img/lis3781_a1_q5.png)
#### Query Result Set 6
  ![Screenshot of back-eng queries](img/lis3781_a1_q6pt1.png)
  ![Screenshot of back-eng queries](img/lis3781_a1_q6pt2.png)
#### Query Result Set 7
  ![Screenshot of back-eng queries](img/lis3781_a1_q7.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
