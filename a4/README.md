> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### Assignment 4 Requirements:


1.  Provide screenshot of your ERD.
2.  Generate an ERD in MS SQL Server from the statements.
3.  Optional: SQL code for the required reports.
4.  Populate the tables with appropriate amount of data.


#### Screenshot of ERD generated from the MS SQL Server environment using the SQL statements.

![Screenshot of ERD for a4](img/lis3781_a4_digraam.png)

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
