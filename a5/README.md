> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### Assignment 5 Requirements:


1. Screenshot of *your* ERD;
2. Screenshot: At least *one* required report (i.e., exercise below), and SQL code solution.
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link


#### Screenshot of ERD generated from the MS SQL Server environment using the SQL statements.

![Screenshot of ERD for a5](img/lis3781_diagram_a5.png)
![Screenshot of repor for a5](img/lis3781_a5_repor.png)

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
