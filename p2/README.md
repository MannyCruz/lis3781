> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### P2 Requirements:

1. Requirements:

  *Import data into the collection: Place primer-dataset.json in bin directory!*
  In system shell or command prompt (or Terminal), use mongoimport to insert the documents into the
  restaurants collection in the test database. If the collection already exists in the test database, the
  operation will drop the restaurants collection first.

  *FIRST*: *MUST* cd to mongodb/bin directory!


#### README.md file should include the following items:

* Screenshots of lis4381 Screenshot of at least one MongoDB shell command(s), (e.g., show collections); *
* Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution. *
* Bitbucket repo links: *Your* lis3781 Bitbucket repo link *

#### Assignment Screenshots:



#### Screenshot of lis3781 show collections;

![Show collections](img/lis3781_show_collections.png "Show collections for mongoDB")

#### Screenshot of lis3781 required repo and JSON;

![Required repo and json](img/lis3781_required_report_json.png "Req repo and json")

#### Screenshot of find statement on mongoDB;

![Extra mongo statement](img/lis3781_mongo.png "Extra mongo")
