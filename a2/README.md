> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Emmanuel Cruz

### Assignment 2 Requirements:

*Three Parts:*

1.  Provide screenshots of sql code
2.  Provide screenshots of populated company and customer tables
3.  Create 2 users with distinct privileges


## A2 Database criteria to meet:

1. Limit user1 to select, update, and delete privileges on company and customer tables

2. Limit user2 to select, and insert privileges on customer table

*Log into local server as required:*

3. Verify database/table permissions, show grants:
a. yours/admin
b. user1 (logged in as user1)
c. user2 (logged in as admin)
4. Display current user2 (logged in as user2) and MySQL version
5. List tables (as admin)
6. Display structures for both tables (as admin)
a. company
b. customer
7. Display data for both tables:
a. company (as user2)
b. customer (as user1)
8. Log in as user1:
a. show the SQL INSERT statement, and corresponding query result set that prevented user1
from inserting data in the company table
b. show the SQL INSERT statement, and corresponding query result set that prevented user1
from inserting data in the customer table
9. Log in as user2:
a. show the SQL statement, and corresponding query result set that prevented user2 from
“seeing” company table:
b. same as above, though, prevented from being able to delete from the customer table:
10. Log in as admin: remove both tables (structure and data), and show commands:
11. NOTE: *MUST* include Bitbucket repo link.



  **README.md file should include the following items:**


#### Screenshots of A2 (1-10)
#### Screenshots of sql code and company/customer list_tables_admin

#### Company Table
  ![Screenshot of company table](img/a2_company_table.png)
#### Customer Table
  ![Screenshot of company table](img/a2_customer_table.png)
#### A2 Show Grants user1
  ![Screenshot of grants user1](img/a2_show_grants_user1.png)
#### A2 Show Grants user2
  ![Screenshot of grants user2](img/a2_show_grants_user2.png)
#### List tables as admin
  ![Screenshot of tables](img/list_tables_admin.png)
#### Logged in as user2
  ![Screenshot of login as user2](img/a2_logged_in_as_user2.png)
#### A2 describe company
  ![Screenshot of describe company](img/a2_describe_company.png)
#### A2 describe customer
  ![Screenshot of describe customer](img/a2_describe_customer.png)
#### Select command denied for user2
  ![Screenshot of select cmd denied user2](img/a2_select_command_denied_user2.png)
#### Select command denied for user1
  ![Screenshot of select cmd denied user2](img/a2_select_cmd_denied_user1.png)
#### Insert command denied for user1
  ![Screenshot of insert cmd denied user1](img/a2_insert_denied_user1.png)
#### Insert denied for customer table
  ![Screenshot of insert cmd denied for customer table](img/a2_insert_denied_for_customer.png)
#### Select denied for user2 on company table
  ![Screenshot of select cmd denied for usr2 company table](img/a2_select_denied_user2_company.png)
#### Delete command denied
  ![Screenshot of delete command denied](img/a2_delete_denied.png)
#### Remove both tables
  ![Screenshot of removal of both tables](img/a2_remove_both_tables.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
